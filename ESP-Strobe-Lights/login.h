/*
 * Copyright By Kris Occhipinti April 8th 2021
 * Code license GPLv3
 * https://www.gnu.org/licenses/gpl-3.0.txt
 * or see Notes.h for License
 */
String responseHTML="<!DOCTYPE html>"
"<html lang='en'>"
"<head>"
"<title>Hello Strobes</title>"
"<meta charset='utf-8'>"
"<meta name='viewport' content='width=device-width, initial-scale=1'>"
"<style>"
"html *{"
"font-size: 35px !important;"
"margin: 2px;"
"}"
"@media only screen and (min-width: 600px) {"
"body {"
"margin-right:200px;"
"margin-left:200px;"
"margin-top: 0;"
"}"
"}"
".flex{"
"display: grid;"
"grid-row-gap: 1rem;"
"grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));"
"}"
".button-red {"
"box-shadow:inset 0px 1px 0px 0px #cf866c;"
"background:linear-gradient(to bottom, #d0451b 5%, #bc3315 100%);"
"background-color:#d0451b;"
"border-radius:3px;"
"border:1px solid #942911;"
"display:inline-block;"
"cursor:pointer;"
"color:#ffffff;"
"font-family:Arial;"
"font-size:13px;"
"padding:6px 24px;"
"text-decoration:none;"
"text-shadow:0px 1px 0px #854629;"
"text-align:center;"
"}"
".button-red:hover {"
"background:linear-gradient(to bottom, #bc3315 5%, #d0451b 100%);"
"background-color:#bc3315;"
"}"
".button-red:active {"
"position:relative;"
"top:1px;"
"}"
".button {"
"box-shadow:inset 0px 1px 0px 0px #54a3f7;"
"background:linear-gradient(to bottom, #007dc1 5%, #0061a7 100%);"
"background-color:#007dc1;"
"border-radius:3px;"
"border:1px solid #124d77;"
"display:inline-block;"
"cursor:pointer;"
"color:#ffffff;"
"font-family:Arial;"
"font-size:13px;"
"padding:6px 24px;"
"text-decoration:none;"
"text-shadow:0px 1px 0px #154682;"
"}"
".button:hover {"
"background:linear-gradient(to bottom, #0061a7 5%, #007dc1 100%);"
"background-color:#0061a7;"
"}"
".button:active {"
"position:relative;"
"top:1px;"
"}"
"</style>"
"<script>"
"function get(url, success) {"
"var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');"
"xhr.open('GET', url);"
"xhr.onreadystatechange = function() {"
"if (xhr.readyState>3 && xhr.status==200) success(xhr.responseText);"
"};"
"xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');"
"xhr.send();"
"return xhr;"
"}"
"function strobe_on(){"
"get('on',function(data){"
"console.log(data);"
"});"
"}"
"function strobe_off(){"
"get('off', function(data){"
"console.log(data);"
"});"
"}"
"</script>"
"</head>"
"<body>"
"<div class='flex'>"
"<h1>Strobe Controls</h1>"
"</div>"
"<div class='flex'>"
"<button class='button' onclick='strobe_on()'>ON</button>"
"<button class='button-red' onclick='strobe_off()'>OFF</button>"
"</div>"
"</body>"
"</html>";
